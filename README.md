**THIS REPOSITORY IS MODIFIED FROM THE [ORIGINAL](https://github.com/jakevdp/PythonDataScienceHandbook) TO INTEGRATE WITH THE CCI TESTBED**

# Python Data Science Handbook

## Objective
This repository provides experiential learning modules for how to perform basic data science methods & techniques on the CCI Testbed.


## Getting Started
The interactive python notebooks contained in this repository may be run in many different types of environments/platforms:
1. [Running on the CCI Testbed via JupyterHub](#Running_on_the_CCI_Testbed_via_JupyterHub)
1. [Running Locally](#Running_Locally)


#### Running on the CCI Testbed via JupyterHub
**Pre-Requisites**
1. You have a CCI Testbed Account. If not, email the following link requesting an account [support@cci-ai-testbed.atlassian.net](support@cci-ai-testbed.atlassian.net)
1. You have setup two-factor authentication.  If not, follow the tutorial located [here](https://code.vt.edu/cci-ai-testbed/documentation/login).

**Procedure**
1. Navigate to [JupyterHub](https://jupyterhub.apps.ncr1.aitb.cyberinitiative.org)
1. Create a new terminal
1. Clone this repository:
```
$ git clone --recurse-submodules https://code.vt.edu/cci-ai-testbed/documentation
```
1. Navigate to the python data science handbook experiential learning module and update the submodule:
```
$ cd experiential_learning/python-data-science-handbook
$ git submodule init
$ git submodule update
```
1. Create an anaconda environment:
```
$ conda create -n PDSH python=3.6 --file requirements.txt
```
1. Activate your python environment:
```
$ conda activate PDSH
```
1. Install dependencies this repository requires:
```
$ conda install --file requirements.txt
```
1. Navigate to the jupyter notebook via the jupyter browser and run the code.

#### Running Locally
**Pre-Requisites**
1. You are running Linux OS.  Windows is not supported at this time.
1. Anaconda is installed.  If not, follow the directions located [here](https://code.vt.edu/cci-ai-testbed/documentation/host_access/install_anaconda) to install Anaconda on your Linux OS.

**Procedure**
1. Clone this repository:
```
$ git clone --recurse-submodules https://code.vt.edu/cci-ai-testbed/documentation
```
1. Navigate to the python data science handbook experiential learning module and update the submodule:
```
$ cd experiential_learning/python-data-science-handbook
$ git submodule init
$ git submodule update
```
1. Create an anaconda environment:
```
$ conda create -n PDSH python=3.5 --file requirements.txt
```
1. Activate your python environment:
```
$ conda activate PDSH
```
1. Install dependencies this repository requires:
```
$ conda install --file requirements.txt
```
1. Navigate to the jupyter notebook via the jupyter browser and run the code.



## About

This repository contains the entire [Python Data Science Handbook](http://shop.oreilly.com/product/0636920034919.do), in the form of (free!) Jupyter notebooks.

The book was written and tested with Python 3.5, though other Python versions (including Python 2.7) should work in nearly all cases.

The book introduces the core libraries essential for working with data in Python: particularly [IPython](http://ipython.org), [NumPy](http://numpy.org), [Pandas](http://pandas.pydata.org), [Matplotlib](http://matplotlib.org), [Scikit-Learn](http://scikit-learn.org), and related packages.
Familiarity with Python as a language is assumed; if you need a quick introduction to the language itself, see the free companion project,
[A Whirlwind Tour of Python](https://github.com/jakevdp/WhirlwindTourOfPython): it's a fast-paced introduction to the Python language aimed at researchers and scientists.

See [Index.ipynb](http://nbviewer.jupyter.org/github/jakevdp/PythonDataScienceHandbook/blob/master/notebooks/Index.ipynb) for an index of the notebooks available to accompany the text.



## License

### Code
The code in this repository, including all code samples in the notebooks listed above, is released under the [MIT license](LICENSE-CODE). Read more at the [Open Source Initiative](https://opensource.org/licenses/MIT).

### Text
The text content of the book is released under the [CC-BY-NC-ND license](LICENSE-TEXT). Read more at [Creative Commons](https://creativecommons.org/licenses/by-nc-nd/3.0/us/legalcode).
